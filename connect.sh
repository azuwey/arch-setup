#!/bin/bash

export DEFAULT_IP=192.168.122.125

read -p "What is the ip address of the server (Default: $DEFAULT_IP): " IP
if [[ $IP == "" ]]
then
  IP=$DEFAULT_IP
fi

scp -oStrictHostKeyChecking=no -oCheckHostIP=no ${PWD}/setup.sh root@$IP:/root/setup.sh
scp -oStrictHostKeyChecking=no -oCheckHostIP=no ${PWD}/post.sh root@$IP:/root/post.sh

exit 1