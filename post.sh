#!/bin/bash

# Start network manager
NetworkManager

# Update packages
yay -Syu

# Install VSCode
yay -S visual-studio-code-bin

# Install Spotify
yay -S spotify

# Install VLC
yay -S vlc

# Install Firefox Developer Edition
yay -S firefox-developer-edition

# Install Docker group
yay -S docker docker-compose docker-machine
groupadd docker
usermod -aG docker $USER

# Install Latest GCC and CMake
yay -S gcc cmake
