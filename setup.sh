#!/bin/bash

echo "#######################################################"
echo "# WARNING THIS PROCESS WILL ERASE ALL OF YOUR DATA!!! #"
echo "#######################################################"
echo ""

DEFAULT_DISK=/dev/vda
DEFAULT_ROOT_PASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
DEFAULT_ENCRYPT_PASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
DEFAULT_CRYPT_MAP=lvmcrypt
DEFAULT_LUKS_NAME=luks
DEFAULT_USERNAME=default
DEFAULT_USER_PASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
DEFAULT_LOCAL="en_US.UTF-8 UTF-8"
DEFAULT_TIME_ZONE=/usr/share/zoneinfo/Europe/Budapest
DEFAULT_HOME_SIZE=4G
DEFAULT_PROJECTS_SIZE=2G
DEFAULT_OPT_SIZE=6G
DEFAULT_SWAP_SIZE=400M

read -p "Which disk want to use (Default: $DEFAULT_DISK): " SELECTED_DISK
if [[ $SELECTED_DISK == "" ]]
then
  SELECTED_DISK=$DEFAULT_DISK
fi

BOOT_PARTITION=$SELECTED_DISK"1"
LVM_PARTITION=$SELECTED_DISK"2"

read -p "How much disk want to use for the Home dir, if non type -1 (Default: $DEFAULT_HOME_SIZE): " \
  SELECTED_HOME_SIZE
if [[ $SELECTED_HOME_SIZE == "" ]]
then
  SELECTED_HOME_SIZE=$DEFAULT_HOME_SIZE
fi

read -p "How much disk want to use for the Projects dir, if non type -1 (Default: $DEFAULT_PROJECTS_SIZE): " \
  SELECTED_PROJECTS_SIZE
if [[ $SELECTED_PROJECTS_SIZE == "" ]]
then
  SELECTED_PROJECTS_SIZE=$DEFAULT_PROJECTS_SIZE
fi

read -p "How much disk want to use for the Opt dir, if non type -1 (Default: $DEFAULT_OPT_SIZE): " \
  SELECTED_OPT_SIZE
if [[ $SELECTED_OPT_SIZE == "" ]]
then
  SELECTED_OPT_SIZE=$DEFAULT_OPT_SIZE
fi

read -p "How much disk want to use for the swap, if non type -1 (Default: $DEFAULT_SWAP_SIZE): " \
  SELECTED_SWAP_SIZE
if [[ $SELECTED_SWAP_SIZE == "" ]]
then
  SELECTED_SWAP_SIZE=$DEFAULT_SWAP_SIZE
fi

read -s -p "Type root password (Default: $DEFAULT_ROOT_PASS): " SELECTED_ROOT_PASS && echo ""
if [[ $SELECTED_ROOT_PASS == "" ]]
then
  SELECTED_ROOT_PASS=$DEFAULT_ROOT_PASS
fi

read -p "Type username (Default: $DEFAULT_USERNAME): " SELECTED_USERNAME
if [[ $SELECTED_USERNAME == "" ]]
then
  SELECTED_USERNAME=$DEFAULT_USERNAME
fi

read -s -p "Type user password (Default: $DEFAULT_USER_PASS): " SELECTED_USERNAME_PASS && echo ""
if [[ $SELECTED_USERNAME_PASS == "" ]]
then
  SELECTED_USERNAME_PASS=$DEFAULT_USER_PASS
fi

read -s -p "Type disk encryption password (Default: $DEFAULT_ENCRYPT_PASS): " SELECTED_ENCRYPT_PASS && echo ""
if [[ $SELECTED_ENCRYPT_PASS == "" ]]
then
  SELECTED_ENCRYPT_PASS=$DEFAULT_ENCRYPT_PASS
fi

read -p "Type crypt map what you want to use (Default: $DEFAULT_CRYPT_MAP): " SELECTED_CRYPT_MAP
if [[ $SELECTED_CRYPT_MAP == "" ]]
then
  SELECTED_CRYPT_MAP=$DEFAULT_CRYPT_MAP
fi

read -p "Which local you want to choose (Default: $DEFAULT_LOCAL): " SELECTED_LOCAL
if [[ $SELECTED_LOCAL == "" ]]
then
  SELECTED_LOCAL=$DEFAULT_LOCAL
fi

read -p "Which timezone are you in (Default: $DEFAULT_TIME_ZONE): " SELECTED_TIME_ZONE
if [[ $SELECTED_TIME_ZONE == "" ]]
then
  SELECTED_TIME_ZONE=$DEFAULT_TIME_ZONE
fi

read -p "Do you want to continue? N/Y (Default: N): " NEXT
if [[ $NEXT != "Y" ]]
then
  exit 1
fi

# Format disk
read -p "Do you want secure erase? N/Y (Default: Y): " NEXT
if [[ $NEXT == "Y" ]]
then
  shred -vfz -n 10 $SELECTED_DISK
fi

# Create partitions
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk $SELECTED_DISK
  o # clear the in memory partition table
  n # new partition
  p # primary partition
  1 # partition number 1
    # default - start at beginning of disk 
  +400M # 400 MB boot partition
  a # make a partition bootable
  n # create the second partition
  p # primary partition
  2 # partion number 2
    # default, start immediately after preceding partition
    # default, extend partition to end of disk
  t # change the type
  2 # partion number 2
  8E # partion type LinuxLVM
  w # write the partition table
EOF

# Encrypt disk
echo -n $SELECTED_ENCRYPT_PASS | cryptsetup -y -v luksFormat $LVM_PARTITION
echo -n $SELECTED_ENCRYPT_PASS | cryptsetup luksOpen $LVM_PARTITION $SELECTED_CRYPT_MAP -d -

# Create LVM partitions
pvcreate /dev/mapper/$SELECTED_CRYPT_MAP
vgcreate $DEFAULT_LUKS_NAME /dev/mapper/$SELECTED_CRYPT_MAP

if [[ $SELECTED_HOME_SIZE != "-1" ]]
then
  lvcreate -L $SELECTED_HOME_SIZE $DEFAULT_LUKS_NAME -n home
fi

if [[ $SELECTED_PROJECTS_SIZE != "-1" ]]
then
  lvcreate -L $SELECTED_PROJECTS_SIZE $DEFAULT_LUKS_NAME -n projects
fi

if [[ $SELECTED_OPT_SIZE != "-1" ]]
then
  lvcreate -L $SELECTED_OPT_SIZE $DEFAULT_LUKS_NAME -n opt
fi

if [[ $SELECTED_SWAP_SIZE != "-1" ]]
then
  lvcreate -L $SELECTED_SWAP_SIZE $DEFAULT_LUKS_NAME -n swap
fi

lvcreate -l 100%FREE $DEFAULT_LUKS_NAME -n root

# Enable lvm module
modprobe dm_mod

# Scan all disks for VGs and rebuild caches
vgscan

# Activate all known VGs in the system
vgchange -ay

# Format LVs and mount drives
mkfs.ext4 /dev/mapper/luks-root
mount /dev/mapper/luks-root /mnt

# Format boot drive
mkfs.ext2 $BOOT_PARTITION
mkdir /mnt/boot
mount $BOOT_PARTITION /mnt/boot

mkdir /mnt/home
if [[ $SELECTED_HOME_SIZE != "-1" ]]
then
  mkfs.ext4 /dev/mapper/luks-home
  mount /dev/mapper/luks-home /mnt/home
fi

mkdir /mnt/projects
if [[ $SELECTED_PROJECTS_SIZE != "-1" ]]
then
  mkfs.ext4 /dev/mapper/luks-projects
  mount /dev/mapper/luks-projects /mnt/projects
fi

mkdir /mnt/opt
if [[ $SELECTED_OPT_SIZE != "-1" ]]
then
  mkfs.ext4 /dev/mapper/luks-opt
  mount /dev/mapper/luks-opt /mnt/opt
fi

if [[ $SELECTED_SWAP_SIZE != "-1" ]]
then
  mkswap /dev/mapper/luks-swap
  swapon /dev/mapper/luks-swap
fi

# Install base packages
pacstrap /mnt base

# Install bootloader
pacstrap /mnt grub

# Install kernel
pacstrap /mnt linux-headers linux

# Install network manager with applet
pacstrap /mnt networkmanager network-manager-applet

# Install wireless utils
pacstrap /mnt wpa_supplicant wireless_tools blueman

# Install i3wm
pacstrap /mnt i3-wm i3lock i3status nitrogen rofi xcompmgr ranger vim

# Install audio utils with applet
pacstrap /mnt pavucontrol alsa-utils volumeicon 

# Install display manager
pacstrap /mnt lxterminal lxdm lxsession

# Install sudo
pacstrap /mnt sudo
echo "%wheel ALL=(ALL) ALL" >> /mnt/sudoers

# Install Input drivers
pacstrap /mnt xf86-input-keyboard xf86-input-mouse xf86-input-synaptics

# Install YaY
pacstrap /mnt git
arch-chroot /mnt git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si && cd .. && rm -rf yay

# Generate fstab
genfstab -L /mnt >> /mnt/etc/fstab

# Add encryption and lvm to the kernel
for i in /mnt/etc/mkinitcpio.conf; \
  do sed -i 's/block filesystems/block encrypt lvm2 filesystems/g' "$i"; done

# Rebuild kernel
arch-chroot /mnt mkinitcpio -p linux

# Set location and time zone
echo $SELECTED_LOCAL >> /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
arch-chroot /mnt rm /etc/localtime
arch-chroot /mnt ln -s $SELECTED_TIME_ZONE /etc/localtime
arch-chroot /mnt hwclock --systohc --utc
echo "$SELECTED_ROOT_PASS\n$SELECTED_ROOT_PASS\n" | arch-chroot /mnt passwd

# Update grub
sed -i 's|quiet|cryptdevice='"$LVM_PARTITION"':'"$DEFAULT_LUKS_NAME"' quiet|g' /mnt/etc/default/grub
arch-chroot /mnt grub-install --target=i386-pc --recheck $SELECTED_DISK
cp /mnt/usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /mnt/boot/grub/locale/en.mo
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

# Add user to the system
arch-chroot /mnt useradd -m $SELECTED_USERNAME
echo "$SELECTED_USERNAME_PASS\n$SELECTED_USERNAME_PASS\n" | arch-chroot /mnt passwd $SELECTED_USERNAME
arch-chroot /mnt usermod -aG wheel $SELECTED_USERNAME

# Enable graphics
arch-chroot /mnt systemctl enable lxdm
arch-chroot /mnt systemctl set-default graphical.target

# Copy post script to user
cp post.sh /mnt/home/$SELECTED_USERNAME/post.sh

# Unmount
umount /mnt/boot
umount /mnt/home
umount /mnt/projects
umount /mnt/opt
umount /mnt